# frozen_string_literal: true

require 'rspec-parameterized'

module SomeExtension
end

# commenting this out will work
Object.prepend(SomeExtension)

describe 'TableTests' do
  it 'locks up' do
    # changing this to not use block expectations works too
    expect { 'a' }.not_to raise_error
  end

  # changing this to not use TableSyntax will work too
  describe 'table syntax' do
    using RSpec::Parameterized::TableSyntax

    where(:value, :result) do
      :a | 'b'
    end

    with_them do
    end
  end
end
